using System.Net;

namespace MythicTable.Campaign.Exceptions
{
    public class CharacterNotFoundException : CampaignException
    {
        public CharacterNotFoundException(string msg) : base(msg) { }

        public override HttpStatusCode StatusCode => HttpStatusCode.NotFound;
    }
}