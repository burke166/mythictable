using System.Net;

namespace MythicTable.Campaign.Exceptions
{
    public class CharacterNotRemovedException : CampaignException
    {
        public CharacterNotRemovedException(string msg) : base(msg) { }

        public override HttpStatusCode StatusCode => HttpStatusCode.BadRequest;
    }
}