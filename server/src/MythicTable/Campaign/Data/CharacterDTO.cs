using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace MythicTable.Campaign.Data
{
    public class CharacterDTO
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]     
        public string id { get; set; }
        public Token token { get; set; } = new Token();
        public Asset asset { get; set; }

        public class Token
        {
            public Image image { get; set; }
            public string scene { get; set; } = "stronghold";
            public Pos pos { get; set; } = new Pos();

            public class Image
            {
                public string color { get; set; }
                public string asset { get; set; }
                public int width { get; set; }
                public int height { get; set; }
                public Origin origin { get; set; }
                public class Origin
                {
                    public int x { get; set; } = 150;
                    public int y { get; set; } = 150;
                }

                public Image()
                {
                    asset = ".";
                    width = 300;
                    height = 300;
                    origin = new Origin();
                }

                public Image(string color)
                {
                    this.color = color;
                }
            }

            public class Pos
            {
                public double q { get; set; }
                public double r { get; set; }
            }
        }

        public class Asset
        {
            public string kind { get; set; } = "image";
            public string src { get; set; }
        }
    }
}