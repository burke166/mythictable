using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using MythicTable.Campaign.Exceptions;

namespace MythicTable.Campaign.Data
{    
    public class MongoDbCampaignProvider : ICampaignProvider
    {
        private readonly IMongoCollection<CampaignDTO> campaigns;
        private readonly IMongoCollection<CampaignCharacterContainer> campaignCharacters;
        private readonly IMongoCollection<CampaignRollContainer> campaignRolls;

        public MongoDbCampaignProvider(MongoDbSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);
            campaigns = database.GetCollection<CampaignDTO>("campaign");
            campaignCharacters = database.GetCollection<CampaignCharacterContainer>("campaign-characters");
            campaignRolls = database.GetCollection<CampaignRollContainer>("campaign-rolls");
        }
        
        public async Task<List<CampaignDTO>> GetAll()
        {
            return await campaigns.Find(campaign => true).ToListAsync();
        }

        public async Task<CampaignDTO> Get(string campaignId)
        {
            var campaign = await campaigns.Find<CampaignDTO>(campaign => campaign.Id == campaignId).FirstOrDefaultAsync();
            if (campaign == null)
            {
                throw new CampaignNotFoundException($"Cannot find campaign of id {campaignId}");
            }

            return campaign;
        }

        public async Task<CampaignDTO> Create(CampaignDTO campaign, PlayerDTO owner)
        {
            if (campaign == null)
            {
                throw new CampaignInvalidException($"The campaign is null");
            }

            if (campaign.Id != null && campaign.Id.Length != 0)
            {
                throw new CampaignInvalidException($"The Campaign already has an id");
            }

            campaign.Owner = owner.Name;
            await campaigns.InsertOneAsync(campaign);
            await campaignCharacters.InsertOneAsync(new CampaignCharacterContainer{
                Id = campaign.Id,
                Characters = new List<CharacterDTO>()
            });
            await campaignRolls.InsertOneAsync(new CampaignRollContainer{
                Id = campaign.Id,
                Rolls = new List<RollDTO>()
            });
            return campaign;
        }

        public async Task<CampaignDTO> Update(CampaignDTO campaign)
        {
            if (campaign == null)
            {
                throw new CampaignInvalidException($"The campaign is null");
            }

            if (campaign.Id == null || campaign.Id.Length == 0)
            {
                throw new CampaignInvalidException($"The Campaign MUST have an id");
            }

            await campaigns.ReplaceOneAsync(c => c.Id == campaign.Id, campaign);
            return campaign;
        }

        public async Task Delete(string campaignId)
        {
            var campaign = await this.Get(campaignId);
            var results = await campaigns.DeleteOneAsync(campaign => campaign.Id == campaignId);
            if (results.DeletedCount == 0) 
            {
                new CampaignNotFoundException($"Campaign id {campaignId} doesn't exist");
            }
        }

        public async Task<List<PlayerDTO>> GetPlayers(string campaignId)
        {
            var campaign = await this.Get(campaignId);

            if (campaign == null)
            {
                throw new CampaignNotFoundException($"Get Player. Cannot find campaign of id {campaignId}");
            }

            return campaign.Players;
        }
        
        public async Task<CampaignDTO> AddPlayer(string campaignId, PlayerDTO player)
        {
            var campaign = await this.Get(campaignId);
            
            if (campaign == null)
            {
                throw new CampaignNotFoundException($"Add Player. Cannot find campaign of id {campaignId}");
            }
            
            if (campaign.Players.Any(m => m.Name == player.Name))
            {
                throw new CampaignAddPlayerException($"The player '{player.Name}' is already in campaign {campaignId}");
            }

            campaign.Players.Add(new PlayerDTO
            {
                Name = player.Name
            });
            
            await this.Update(campaign);
            return campaign;
        }

        public async Task<CampaignDTO> RemovePlayer(string campaignId, PlayerDTO player)
        {
            var campaign = await this.Get(campaignId);
            if (campaign == null || campaign.Players == null)
            {
                throw new CampaignNotFoundException($"Remove Player. Cannot find campaign of id {campaignId}");
            }

            var numberRemoved = campaign.Players.RemoveAll(membership => membership.Name == player.Name);
            if (numberRemoved == 0)
            {
                throw new CampaignRemovePlayerException($"The player '{player.Name}' is not in campaign {campaignId}");
            }

            await this.Update(campaign);
            return campaign;
        }

        public async Task<List<CharacterDTO>> GetCharacters(string campaignId)
        {
            var campaignCharacter = await campaignCharacters.Find<CampaignCharacterContainer>(cc => cc.Id == campaignId).FirstOrDefaultAsync();
            if (campaignCharacter == null)
            {
                return new List<CharacterDTO>();
            }
            return campaignCharacter.Characters;
        }

        public async Task<CharacterDTO> AddCharacter(string campaignId, CharacterDTO character)
        {
            var campaignCharacter = await campaignCharacters.Find<CampaignCharacterContainer>(cc => cc.Id == campaignId).FirstOrDefaultAsync();
            if (campaignCharacter == null)
            {
                await campaignCharacters.InsertOneAsync(new CampaignCharacterContainer{
                    Id = campaignId,
                    Characters = new List<CharacterDTO>{
                        character
                    }
                });
            }
            else
            {
                character.id = ObjectId.GenerateNewId().ToString();
                await campaignCharacters.UpdateOneAsync(
                    c => c.Id == campaignId,
                    Builders<CampaignCharacterContainer>.Update.Push("Characters", character));
            }
            return character;
        }

        public async Task<List<CharacterDTO>> AddCharacters(string campaignId, List<CharacterDTO> newCharacters)
        {
            var campaignCharacter = await campaignCharacters.Find<CampaignCharacterContainer>(cc => cc.Id == campaignId).FirstOrDefaultAsync();
            foreach(var c in newCharacters)
            {
                c.id = ObjectId.GenerateNewId().ToString();
            }
            if (campaignCharacter == null)
            {
                await campaignCharacters.InsertOneAsync(new CampaignCharacterContainer{
                    Id = campaignId,
                    Characters = newCharacters
                });
            }
            else
            {
                await campaignCharacters.UpdateOneAsync(
                    c => c.Id == campaignId,
                    Builders<CampaignCharacterContainer>.Update.PushEach("Characters", newCharacters));
            }
            return newCharacters;
        }

        public async Task RemoveCharacter(string campaignId, string characterId)
        {
            var update = Builders<CampaignCharacterContainer>
                .Update
                .PullFilter(
                    campaignContainer => campaignContainer.Characters,
                    character => character.id == characterId);
            var result = await campaignCharacters.UpdateOneAsync(campaignContainer => campaignContainer.Id == campaignId, update);
            if(result.ModifiedCount == 0)
            {
                throw new CharacterNotRemovedException($"Could not remove character with id '{characterId}' from campaign ${campaignId}");
            }
        }

        public async Task<long> MoveCharacter(string campaignId, string characterId, double x, double y)
        {
            var filter = Builders<CampaignCharacterContainer>.Filter.Where(x => x.Id == campaignId && x.Characters.Any(i => i.id == characterId));
            var update = Builders<CampaignCharacterContainer>.Update
                .Set(x => x.Characters[-1].token.pos.q, x)
                .Set(x => x.Characters[-1].token.pos.r, y);
            var result = await campaignCharacters.UpdateOneAsync(filter, update);
            return result.ModifiedCount;
        }
        
        public async Task<List<RollDTO>> GetRolls(string campaignId)
        {
            var campaignRollContainer = await campaignRolls.Find<CampaignRollContainer>(cc => cc.Id == campaignId).FirstOrDefaultAsync();
            if (campaignRollContainer == null)
            {
                return new List<RollDTO>();
            }
            return campaignRollContainer.Rolls;
        }
        
        public async Task<RollDTO> AddRoll(string campaignId, RollDTO roll)
        {
            var campaignRollContainer = await campaignRolls.Find<CampaignRollContainer>(cc => cc.Id == campaignId).FirstOrDefaultAsync();
            if (campaignRollContainer == null)
            {
                await campaignRolls.InsertOneAsync(new CampaignRollContainer{
                    Id = campaignId,
                    Rolls = new List<RollDTO>{
                        roll
                    }
                });
            }
            else
            {
                roll.Id = ObjectId.GenerateNewId().ToString();
                await campaignRolls.UpdateOneAsync(
                    c => c.Id == campaignId,
                    Builders<CampaignRollContainer>.Update.Push("Rolls", roll));
            }
            return roll;
        }

        private class CampaignCharacterContainer
        {
            [BsonId]
            [BsonRepresentation(BsonType.ObjectId)]     
            public string Id { get; set; }
            public List<CharacterDTO> Characters { get; set; } = new List<CharacterDTO>();
        }

        private class CampaignRollContainer
        {
            [BsonId]
            [BsonRepresentation(BsonType.ObjectId)]     
            public string Id { get; set; }
            public List<RollDTO> Rolls { get; set; } = new List<RollDTO>();
        }
    }
}
