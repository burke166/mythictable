using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MongoDB.Bson;
using MythicTable.Campaign.Data;

namespace MythicTable.Campaign.Util
{
    public class CharacterUtil
    {
        public static CharacterDTO CreateCharacter(string image, double q, double r)
        {
            return new CharacterDTO{
                token = new CharacterDTO.Token{
                    image =  new CharacterDTO.Token.Image(),
                    pos = new CharacterDTO.Token.Pos{
                        q = q,
                        r = r
                    }
                },
                asset = new CharacterDTO.Asset{
                    src = image
                }
            };
        }
        
        public static CharacterDTO CreateColorToken(string color, double q, double r)
        {
            return new CharacterDTO{
                token = new CharacterDTO.Token{
                    image =  new CharacterDTO.Token.Image(color),
                    pos = new CharacterDTO.Token.Pos{
                        q = q,
                        r = r
                    }
                }
            };
        }
    }
}
