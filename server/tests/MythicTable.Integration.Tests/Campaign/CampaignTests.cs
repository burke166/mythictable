using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using Mongo2Go;
using MythicTable.Integration.Tests.Util;
using MythicTable.Campaign.Data;
using Newtonsoft.Json;
using Xunit;

namespace MythicTable.Integration.Tests
{
    public class CampaignTests : IClassFixture<WebApplicationFactory<MythicTable.Startup>>
    {
        private readonly WebApplicationFactory<MythicTable.Startup> _factory;

        public CampaignTests(WebApplicationFactory<MythicTable.Startup> factory)
        {
            var runner = MongoDbRunner.Start();
            Environment.SetEnvironmentVariable("MTT_MONGODB_CONNECTIONSTRING", runner.ConnectionString);
            Environment.SetEnvironmentVariable("MTT_MONGODB_DATABASENAME", "mythictable");
            _factory = factory;
        }

        [Fact]
        public async Task CreateCampaignRequiresAuthenticationTest()
        {
            var client = _factory.CreateClient();

            var campaign = new CampaignDTO()
            {
                Name = "Integration Test Campaign"
            };

            var cancellationToken = new CancellationToken();
            using (var response = await RequestHelper.PostStreamAsync(client, "/api/campaigns", campaign, cancellationToken))
            {
                Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
            }
        }

        //[Fact]
        private async Task CreateCampaignTest()
        {
            var client = _factory.CreateClient();

            var campaign = new CampaignDTO()
            {
                Name = "Integration Test Campaign"
            };

            var cancellationToken = new CancellationToken();
            using (var response = await RequestHelper.PostStreamAsync(client, "/api/campaigns", campaign, cancellationToken))
            {
                response.EnsureSuccessStatusCode();
                Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType.ToString());
                var json = await response.Content.ReadAsStringAsync();
                var newCampaign = JsonConvert.DeserializeObject<CampaignDTO>(json);
                Assert.Equal("Integration Test Campaign", newCampaign.Name);
            }
        }
    }
}