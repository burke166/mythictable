using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MythicTable;
using MythicTable.Campaign.Data;
using MythicTable.Campaign.Exceptions;
using MythicTable.Tests.Util;
using Xunit;

namespace MythicTable.Tests.Controllers
{
    public class InMemoryCampaignProviderTests : IAsyncLifetime
    {
        private ICampaignProvider provider;
        private const string DoesntExistId = "551137c2f9e1fac808a5f572";
        public Task InitializeAsync()
        {
            provider = new InMemoryCampaignProvider();
            return Task.CompletedTask;
        }

        public Task DisposeAsync()
        {
            return Task.CompletedTask;
        }

        [Fact]
        public async void TestCreateCampaignAsync()
        {
            var campaign = new CampaignDTO{Name = "Test"};
            var owner = new PlayerDTO { Name = "test player" };

            var resultCampaign = await provider.Create(campaign, owner);
            Assert.Equal(campaign.Name, resultCampaign.Name);
            Assert.Equal(owner.Name, resultCampaign.Owner);
        }

        [Fact]
        public async void TestNullCampaignThrowsExceptionOnCreateAsync()
        {
            await Assert.ThrowsAsync<CampaignInvalidException>(() => provider.Create(null, new PlayerDTO { Name = "test player" }));
        }

        [Fact]
        public async void TestCampaignWithIdThrowsExceptionOnCreateAsync()
        {
            await Assert.ThrowsAsync<CampaignInvalidException>(() => provider.Create(new CampaignDTO{Id = DoesntExistId}, new PlayerDTO { Name = "test player" }));
        }

        [Fact]
        public async void TestUpdateAsync()
        {
            var testCampaign = await CreateCampaign();
            testCampaign.Name = "Modified";

            var resultingCampaign = await provider.Update(testCampaign);
            
            Assert.Equal(testCampaign.Name, resultingCampaign.Name);
        }

        [Fact]
        public async void TestNullCampaignThrowsExceptionOnUpdateAsync()
        {
            await Assert.ThrowsAsync<CampaignInvalidException>(() => provider.Update(null));
        }

        [Fact]
        public async void TestCampaignWithoutIdThrowsExceptionOnUpdateAsync()
        {
            await Assert.ThrowsAsync<CampaignInvalidException>(() => provider.Update(new CampaignDTO{Name = "test"}));
        }

        [Fact]
        public async void TestGetCampaignAsync()
        {
            var testCampaign = await CreateCampaign();

            var results = await provider.Get(testCampaign.Id);
            Assert.Equal(testCampaign.Name, results.Name);
            Assert.Equal(testCampaign.Id, results.Id);
        }

        [Fact]
        public async void TestGetReturnsNoCampaignsAsync()
        {
            await Assert.ThrowsAsync<CampaignNotFoundException>(() => provider.Get(DoesntExistId));
        }

        [Fact]
        public async void TestGetAllCampaignsAsync()
        {
            var results1 = await provider.GetAll();
            var testCampaign1 = await CreateCampaign();
            var testCampaign2 = await CreateCampaign();
            var results2 = await provider.GetAll();

            Assert.Equal(2, results2.Count - results1.Count);
            Assert.Equal(testCampaign1.Id, results2[results2.Count-2].Id);
            Assert.Equal(testCampaign1.Name, results2[results2.Count-2].Name);
            Assert.Equal(testCampaign2.Id, results2[results2.Count-1].Id);
            Assert.Equal(testCampaign2.Name, results2[results2.Count-1].Name);
        }

        [Fact]
        public async void TestDeleteAsync()
        {
            var testCampaign = await CreateCampaign();
            await provider.Delete(testCampaign.Id);
            await Assert.ThrowsAsync<CampaignNotFoundException>(() => provider.Get(testCampaign.Id));
        }

        [Fact]
        public async void TestDeleteInvalidIdFailsAsync()
        {
            await Assert.ThrowsAsync<CampaignNotFoundException>(() => provider.Delete(DoesntExistId));
        }

        [Fact]
        public async void TestPlayersAreEmptyWithNewCampaignAsync()
        {
            var testCampaign = await CreateCampaign();
            var result = await provider.GetPlayers(testCampaign.Id);
            Assert.Empty(result);
        }

        [Fact]
        public async void TestAddPlayersToCampaignAsync()
        {
            var testCampaign = await CreateCampaign();
            var campaign = await provider.AddPlayer(testCampaign.Id, new PlayerDTO{Name = "test player"});
            Assert.Single(campaign.Players);
        }

        [Fact]
        public async void TestAddPlayerTwiceCausesErrorAsync()
        {
            var testCampaign = await CreateCampaign();
            await provider.AddPlayer(testCampaign.Id, new PlayerDTO{Name = "test player"});
            await Assert.ThrowsAsync<CampaignAddPlayerException>(() => provider.AddPlayer(testCampaign.Id, new PlayerDTO{Name = "test player"}));
        }

        [Fact]
        public async void TestRemovingPlayerFromEmptyCampaignCausesErrorAsync()
        {
            var testCampaign = await CreateCampaign();
            await Assert.ThrowsAsync<CampaignRemovePlayerException>(() => provider.RemovePlayer(testCampaign.Id, new PlayerDTO{Name = "test player"}));
        }

        [Fact]
        public async void TestRemovingPlayerRemovesThemFromCampaignAsync()
        {
            var testCampaign = await CreateCampaign();
            var player = new PlayerDTO{Name = "test player"};
            await provider.AddPlayer(testCampaign.Id, player);
            await provider.RemovePlayer(testCampaign.Id, player);
            await Assert.ThrowsAsync<CampaignRemovePlayerException>(() => provider.RemovePlayer(testCampaign.Id, player));
        }

        [Fact]
        public async void TestRemovingWrongPlayerCausesErrorAsync()
        {
            var testCampaign = await CreateCampaign();
            var response = await provider.AddPlayer(testCampaign.Id, new PlayerDTO{Name = "test player"});
            await Assert.ThrowsAsync<CampaignRemovePlayerException>(() => provider.RemovePlayer(testCampaign.Id, new PlayerDTO{Name = "WRONG player"}));
        }

        [Fact]
        public async void TestPlayersShowInCampaignAsync()
        {
            var testCampaign = await CreateCampaign();
            var player = new PlayerDTO{Name = "test player"};
            await provider.AddPlayer(testCampaign.Id, player);

            var results = await provider.Get(testCampaign.Id);
            
            Assert.Single(results.Players);
        }

        [Fact]
        public async void TestCreateCampaignDoesntCreateCharactersAsync()
        {
            var testCampaign = await CreateCampaign();

            var results = await provider.GetCharacters(testCampaign.Id);
            
            Assert.Empty(results);
        }

        [Fact]
        public async void TestAddCharacterAsync()
        {
            var testCampaign = await CreateCampaign();

            var character = new CharacterDTO();
            await provider.AddCharacter(testCampaign.Id, character);
            var results = await provider.GetCharacters(testCampaign.Id);
            
            Assert.Single(results);
        }

        [Fact]
        public async void TestAddCharactersAsync()
        {
            var testCampaign = await CreateCampaign();

            var characters = new List<CharacterDTO>(){
                new CharacterDTO(),
                new CharacterDTO()
            };
            await provider.AddCharacters(testCampaign.Id, characters);
            var results = await provider.GetCharacters(testCampaign.Id);
            
            Assert.Equal(2, results.Count);
        }
        
        [Fact]
        public async void TestAddCharacterIsCampaignAwareAsync()
        {
            var testCampaign1 = await CreateCampaign();
            var testCampaign2 = await CreateCampaign();

            var character = new CharacterDTO();
            await provider.AddCharacter(testCampaign1.Id, character);
            var results = await provider.GetCharacters(testCampaign2.Id);
            
            Assert.Empty(results);
        }

        [Fact]
        public async void TestAddCharacterCreatesIdsAsync()
        {
            var testCampaign = await CreateCampaign();

            var character = new CharacterDTO();
            Assert.Null(character.id);
            await provider.AddCharacter(testCampaign.Id, character);
            var results = await provider.GetCharacters(testCampaign.Id);
            
            Assert.Single(results);
            Assert.Equal("1", results[0].id);

            character = new CharacterDTO();
            Assert.Null(character.id);
            await provider.AddCharacter(testCampaign.Id, character);
            results = await provider.GetCharacters(testCampaign.Id);
            
            Assert.Equal(2, results.Count);
            Assert.Equal("2", results[1].id);

            var characters = new List<CharacterDTO>(){
                new CharacterDTO(),
                new CharacterDTO()
            };
            await provider.AddCharacters(testCampaign.Id, characters);
            results = await provider.GetCharacters(testCampaign.Id);

            Assert.Equal(4, results.Count);
            Assert.Equal("3", results[2].id);
            Assert.Equal("4", results[3].id);
        }

        [Fact]
        public async void TestRemoveCharacterNonExistantCampaignAsync()
        {
             await Assert.ThrowsAsync<CampaignNotFoundException>(() => provider.RemoveCharacter("garbage id", "garbage id"));
        }

        [Fact]
        public async void TestRemoveNonExistantCharacterAsync()
        {
             var testCampaign = await CreateCampaign();
             
             await Assert.ThrowsAsync<CharacterNotFoundException>(() => provider.RemoveCharacter(testCampaign.Id, "garbage id"));
        }

        [Fact]
        public async void TestRemoveCharacterAsync()
        {
             var testCampaign = await CreateCampaign();
             var characterToAdd = new CharacterDTO();
             CharacterDTO addedCharacter = await provider.AddCharacter(testCampaign.Id, characterToAdd);
             var charactersBeforeDelete = await provider.GetCharacters(testCampaign.Id);
             Assert.Single(charactersBeforeDelete);
             Assert.Equal(addedCharacter.id, charactersBeforeDelete[0].id);
             
             await provider.RemoveCharacter(testCampaign.Id, addedCharacter.id);

             var charactersAfterDelete = await provider.GetCharacters(testCampaign.Id);
             Assert.Empty(charactersAfterDelete);
        }

        [Fact]
        public async void TestMoveCharacterAsync()
        {
            var testCampaign = await CreateCampaign();

            var character = new CharacterDTO();
            character = await provider.AddCharacter(testCampaign.Id, character);
            var numModified = await provider.MoveCharacter(testCampaign.Id, character.id, 1, 2);
            
            Assert.Equal(1, numModified);
            var characters = await provider.GetCharacters(testCampaign.Id);
            Assert.Equal(1, characters[0].token.pos.q);
            Assert.Equal(2, characters[0].token.pos.r);
        }

        [Fact]
        public async void TestMoveCharacterNoCampaignThrowsExceptionAsync()
        {
            await Assert.ThrowsAsync<CampaignNotFoundException>(() => provider.MoveCharacter("no-camp", "no-char", 1, 2));
        }

        [Fact]
        public async void TestMoveCharacterNoCharacterThrowsExceptionAsync()
        {
            var testCampaign = await CreateCampaign();
            await Assert.ThrowsAsync<CharacterNotFoundException>(() => provider.MoveCharacter(testCampaign.Id, "no-char", 1, 2));
        }

        [Fact]
        public async void TestAddRollAsync()
        {
            var testCampaign = await CreateCampaign();

            var roll = new RollDTO();
            await provider.AddRoll(testCampaign.Id, roll);
            var results = await provider.GetRolls(testCampaign.Id);
            
            Assert.Single(results);
        }
        
        private async Task<CampaignDTO> CreateCampaign()
        {
            return await provider.Create(new CampaignDTO{Name = Helpers.RandomString(8)}, new PlayerDTO { Name = "test owner" });
        }
    }
}