import DiceRoller from '@/render/diceroller/DiceRoller';
import LivePlayState from '@/live/LivePlayState';

import Vuex from 'vuex';
import { shallowMount, createLocalVue } from '@vue/test-utils';

const localVue = createLocalVue();
localVue.use(Vuex);

describe.skip('DiceRoller', () => {
    let store;
    let roller;
    beforeEach(() => {
        store = new Vuex.Store({
            modules: {
                live: LivePlayState,
            },
        });
        store.commit('live/setUserId', '123');
        store.commit('live/setSessionId', '456');

        roller = shallowMount(DiceRoller, { store, localVue }).vm;
    });
    it('has a formula', () => {
        const defaultFormula = roller.$data.formula;
        expect(defaultFormula).toBe('');
    });
    it('calls the submitRoll function with the correct client and session id', () => {
        const submitRoll = jest.fn();
        roller.$store.commit('live/setDirector', {
            submitRoll: submitRoll,
        });
        roller.rollDice();
        const lastCall = submitRoll.mock.calls[0][0];
        expect(lastCall.userId).toBe('123');
        expect(lastCall.sessionId).toBe('456');
    });
    it('calls the submitRoll function with the correct formula', () => {
        const submitRoll = jest.fn();
        roller.$store.commit('live/setDirector', {
            submitRoll: submitRoll,
        });

        roller.$data.formula = '1d5';
        roller.rollDice();
        const lastCall = submitRoll.mock.calls[0][0];
        expect(lastCall.formula).toBe('1d5');
    });
});
