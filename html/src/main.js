﻿import Vue from 'vue';
import router from './router.js';
import store from './store.js';
import VueGtag from 'vue-gtag';
import axios from 'axios';
import * as VueWindow from '@hscmap/vue-window';

import GameStateStore from './store/GameStateStore';
import actions from './ruleset/experiment/actions';

import './assets/main.scss';

// FIXME: find a better place to load in the ruleset than main.js
const ruleset = {
    actions: actions,
};
GameStateStore.state.ruleset = ruleset;
Vue.use(VueWindow);
Vue.use(VueGtag, {
    config: { id: process.env.VUE_APP_ANALYTICS },
    appName: 'Mythic Table',
    pageTrackerScreenviewEnabled: true,
});

// eslint-disable-next-line no-unused-vars
new Vue({
    el: '#app',
    store,
    router,
    render: h => h('router-view'),
});

// export { store };

// Set up axios interceptor to add API token
// TODO: Restrict this to only add the token if requesting from the original domain
// (so we don't send our SECRET auth token to other websites accidentally)
axios.interceptors.request.use(
    config => {
        if (!(config.hasOwnProperty('disableAuth') && config.disableAuth)) {
            config.headers['Authorization'] = `Bearer ${store.state.oidcStore.access_token}`;
        }
        return config;
    },
    error => {
        return Promise.reject(error);
    },
);
