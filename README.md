# Mythic Table

**Welcome to Mythic Table!<br/>A virtual tabletop application for playing games with your friends online.<br/>If this is your first time here, read more on our website: [https://mythictable.com/](https://mythictable.com/)**

# Quick Start 

If the following doesn't make sense, please contact someone in our [Slack](https://mythictable.slack.com). We'll help you out the best we can! <br/>Many who find this page first will be software developers so we made some assumptions.

## First time setup
1. [Install .NET Core SDK, version 3.1 or higher](https://dotnet.microsoft.com/download) (backend)
2. Install node.js using Visual Studio Installer or [nodejs.org](https://nodejs.org/en/download/) (frontend)
3. [Install Powershell](https://docs.microsoft.com/en-us/powershell/scripting/install/installing-windows-powershell?view=powershell-6) (startup script) *Don't want to install Powershell? See [here](##Non-Powershell-startup)*
4. Open a Powershell window
5. Clone the mythicauth repository (`git clone https://gitlab.com/mythicteam/mythicauth.git`)
6. Open another Powershell window
7. Clone the main repository (`git clone https://gitlab.com/mythicteam/mythictable.git`)

## Launching the app
1. If you think the packages have changed (or this is your first time setting up Mythic Table) open a powershell window and run npm install under `mythictable\html`
2. Open a powershell window 
3. Run `git fetch`, `git checkout master`, and `git pull` from the main `mythictable`
4. Open a Powershell window
5. Run `dotnet run` from the `mythicauth\src\MythicAuth` folder to launch the auth service
6. Open another Powershell window 
7. If it is your first time run `Start-DevEnv.ps1 -IsFirstTime` from the main `mythictable` folder to launch the main application. If you are returning to work on Mythic Table instead run `Start-DevEnv.ps1`. 

## Confirming it is working
If everything is running properly you should be able to open a web browser tab and enter `localhost:5000` to visit the landing page

# Working with MongoDB

The above steps will be enough to get you up and going with an in-memory
database, but in some cases, that's not good enough. This is especially
true if you find you're restarting the server a lot in the process of writing
code or troubleshooting. In cases like this, it might be better to run the
services with a local instance of MongoDB. There are a number of ways to 
do this, but this is the Mythic Table recommended approach:

## Install Docker

Docker is a container system that allows applications to be run in an enviroment
independant of your operating system. Mythic Table uses Docker for a number
of things including our production environment, but we've been shying away from
in for local development environments. The reason is that we want to keep
things as simple as possible and though Docker is not complicated, it does
remain a possible point of failure. However, do not be disheartened, it is
extremely easy to set up and use. Especially with a set of commands to copy
and paste. ;)

### Windows and Mac users

[Docker Desktop](https://www.docker.com/products/docker-desktop) is a great tool
to get started with. Just go [here](https://www.docker.com/products/docker-desktop)
and follow the instructions.

### Linux users

This is much easier for Linux users.
* [Centos](https://docs.docker.com/install/linux/docker-ce/centos/)
* [Debian](https://docs.docker.com/install/linux/docker-ce/debian/)
* [Fedora](https://docs.docker.com/install/linux/docker-ce/fedora/)
* [Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/)

This is an optional step, but highly recommended:
* [Enable Docker CLI for the non-root user](https://docs.docker.com/install/linux/linux-postinstall/#manage-docker-as-a-non-root-user)
account that will be used to run VS Code.

### VS Code Users

* Get the [Docker Extension](https://github.com/microsoft/vscode-docker)
* You can also just search for it under extensions.

## Docker Registry

Docker uses a public image registry to host common images. You will likely need to make an account [here](https://hub.docker.com/)

Then you will login using:
* `docker login`

## The MongoDB Image

* `docker pull mongo`
* `docker run -d --rm -p 27017-27019:27017-27019 --name mongodb mongo:latest`

What do these commands do?

* `docker` is the CLI tool for docker.  You can `docker --help` for more information
* `pull` will pull the image from Docker Hub
* `run` will run the image
* `-d` specifies daemon mode so the image is run in the background
* `--rm` is a nice feature to remove the image after it's terminated
* `-p 27017-27019:27017-27019` instructs docker how to listen and forward ports
* `--name mongodb` creates a user friendly name
* `mongo:latest` the last argument is the image name and tag

You may wish to stop and start later the mongodb server (or docker itself). You can stop and start the mongodb image by doing the following:
* `docker stop mongo`
* `docker start mongo`

## Setting up MongoDB

MongoDB requires a specific user with a specific password for local development.
These steps will walk you through the process of setting that up.

* `docker exec -it mongo mongo`

What's happenging?
* `exec` execute something in the docker container
* `-it` interactively
* `mongo` the first mongo is your image name
* `mongo` the second is your command to run.

You should now have a mongo prompt
```
>
```

From here run the following:

```
use admin
db.createUser(
  {
        user: "admin",
        pwd: "abc123!",
        roles: [ { role: "root", db: "admin" } ]
  }
);
exit;
```

## Running the server to connect to MongoDB

By default the server uses an in-memory collection for data persistance. To
get it to use mongo, add `--launch-profile local-mongo`. This will instruct it to 
use the right profile.
* `dotnet run --project .\server\src\MythicTable\MythicTable.csproj --launch-profile local-mongo`

or

* `cd server/src/MythicTable/`
* `dotnet run --launch-profile local-mongo`
